﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

public class Vibraciones : MonoBehaviour
{
    public HapticTypes haptic_type;
    public float tiempo = 0;
    private bool activo = true;

    //// Setup
    //public float intensity = 1f;
    //public float sharpness = 1f;

    private void Start()
    {
        MMVibrationManager.SetHapticsActive(true);
    }
    public void vibracion1()
    {
        if (activo)
        {
            activo = false;
            MMVibrationManager.Haptic(haptic_type, false, false, this);
            Invoke("activar", tiempo);
        }     
    }
    public void activar()
    {
        activo = true;
    }

    //public void vibracion2()
    //{
    //    MMVibrationManager.TransientHaptic(intensity, sharpness, true, this);
    //}
}
