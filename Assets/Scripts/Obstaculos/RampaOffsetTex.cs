﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RampaOffsetTex : MonoBehaviour
{
    public float scrollSpeedX = 0.0f;
    public float scrollSpeedY = 0.5f;
    public Renderer rend;

    void Start()
    {
        rend = GetComponent<Renderer>();
    }

    void Update()
    {
        float offsetx = Time.time * scrollSpeedX;
        float offsety = Time.time * scrollSpeedY;
        rend.materials[1].SetTextureOffset("_BaseMap", new Vector2(0.4f, offsety));
    }
}

