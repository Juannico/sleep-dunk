using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class MovimientoEsfera : MonoBehaviour
{
    [HideInInspector]
    public Rigidbody esfera;

    // Movimiento 
    private Vector3 pos_ini_clic, pos_fin_clic;
    private float unidad_pantalla_ajustada;
    private bool ini_clic = false;
    private float valor_vector_angulo_lanzamiento_base;

    private bool activador_movimiento = false;
    private bool mepuedomover = true;
    private bool mostrarflecha = false;

    // Control
    public ControlNivel controlNivel;
    public ControlJuego controljuego;
    public PersonajeControl personaje_control;

    // Control Fuerza
    [HideInInspector]
    public float fuerzaActual;
    private float fuerzaBase;
    private float porcentajeSalto;
    private float porcentajeReduccion;
    private float distacia_del_suelo;
    private float angulo_lanzamiento_base;

    // Evento de llamado feel halar
    private bool yallamefxhalar = false;

    // Para cambiar canvas la primera vez que se toca 
    private bool miprimeravez = true;

    private bool activarflecha;

    // Multiplicador 
    private bool yachoquemultiplicador = false;

    private bool vibrar_colision = false;
    private bool puedotocarrampa = true;


    /////////////////////////////////// Colisi�n /////////////////////////////////////////////////////////////
    public void OnTriggerEnter(Collider other)
    {
        // Final 2
        if (other.gameObject.layer == 24)
        {
            personaje_control.control_vibraciones.meta.vibracion1();
            StopAllCoroutines();
            mepuedomover = false;
            if (!controljuego.perdi)
            {
                controlNivel.canvas_victoria.SetActive(true);
                controlNivel.canvas_principal.SetActive(false);
                personaje_control.eventosfeel.dormirallelgaralameta();
            }
            else
            {
                controlNivel.canvas_perdida.SetActive(true);
            }
            controlNivel.actualizarMonedas();
            personaje_control.gamanager.OnLevelComplete(controljuego.nivelActual + 1);

        }
        // Final 1
        if (other.gameObject.layer == 11)
        {
            personaje_control.control_vibraciones.meta.vibracion1();
            if (!controljuego.perdi)
            {
                controlNivel.yaganoelpersonaje = true;
                controlNivel.colocarMonedas(100);
                controlNivel.txt_monedas_nivel.text = (controljuego.monedas_totales + 100).ToString();
                personaje_control.eventosfeel.generarMonedas();
            }
            else
            {
                controlNivel.colocarMonedas(20);
                controlNivel.actualizarMonedas();
                controlNivel.canvas_perdida.SetActive(true);
                StopAllCoroutines();
                mepuedomover = false;
            }
            if (controljuego.pos == 2)
            {
                controlNivel.txt_puesto.text = controljuego.pos.ToString() + "nd Place";
            }
            else if (controljuego.pos == 3)
            {
                controlNivel.txt_puesto.text = controljuego.pos.ToString() + "rd Place";
            }
            else if (controljuego.pos >= 4)
            {
                controlNivel.txt_puesto.text = controljuego.pos.ToString() + "th Place";
            }
        }
        // Rampa
        else if (other.gameObject.layer == 12)
        {
            if (puedotocarrampa)
            {
                esfera.velocity = Vector3.zero;
                puedotocarrampa = false;
                personaje_control.despegarObjetos();
                lanzarPorRampa();
                Invoke("activartocarrampa", 1f);
                GameObject instanciado = Instantiate(personaje_control.eventosfeel.par_rampa, new Vector3(other.transform.position.x, other.transform.position.y, other.transform.position.z + 10), Quaternion.identity) as GameObject;
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        // Charco malo
        if (collision.gameObject.layer == 16)
        {
            if (!personaje_control.tengoPiscinaDentro(collision.gameObject))
            {
                personaje_control.manchar();
                personaje_control.piscinasTocadas.Add(collision.gameObject);
                personaje_control.control_vibraciones.mancha.vibracion1();
            }
        }
        // Charco Agua
        else if (collision.gameObject.layer == 17)
        {
            personaje_control.despegarObjetos();
            if (!personaje_control.tengoPiscinaDentro(collision.gameObject))
            {
                personaje_control.aumentarPorTocarAgua();
                personaje_control.piscinasTocadas.Add(collision.gameObject);
            }
        }
        // Piscina Buena
        else if (collision.gameObject.layer == 19)
        {
            personaje_control.despegarObjetos();
            if (!personaje_control.tengoPiscinaDentro(collision.gameObject))
            {
                personaje_control.caerEnPiscinaAgua();
                personaje_control.piscinasTocadas.Add(collision.gameObject);
                personaje_control.control_vibraciones.piscina.vibracion1();
            }
        }
        // Piscina mala
        else if (collision.gameObject.layer == 18)
        {
            if (!personaje_control.tengoPiscinaDentro(collision.gameObject))
            {
                personaje_control.caerEnPiscinaMancha();
                personaje_control.piscinasTocadas.Add(collision.gameObject);
            }
        }
        else // Piso
        if (collision.gameObject.layer == 13)
        {
            metodoColisionconpiso(null);

        }
        else // Pilares 
        if (collision.gameObject.layer == 25)
        {
            personaje_control.control_vibraciones.choca_con_pilares.vibracion1();
        }

        // Multiplicador 
        else if (collision.gameObject.layer == 23)
        {
            chocarcollisonador(collision);
        }

    }
    /////////////////////////////////// Fin Colisi�n /////////////////////////////////////////////////////////////
    void Start()
    {
        controljuego = GameObject.FindGameObjectWithTag("ControlJuego").GetComponent<ControlJuego>();
        unidad_pantalla_ajustada = Screen.height / 8;
        personaje_control.joystick.SetActive(true);

        controljuego.perdi = false;

        this.GetComponent<Rigidbody>().maxDepenetrationVelocity = 0.01f;
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (mepuedomover)
            {
                if (estacercadelsuelo() || activador_movimiento)
                {
                    mostrarflecha = true;
                    activador_movimiento = false;
                    StopAllCoroutines();
                    StartCoroutine(activarMovimiento(true, 4));

                    personaje_control.joystick.transform.localScale = Vector2.one;
                    pos_ini_clic = Input.mousePosition;
                    ini_clic = true;
                    if (!yallamefxhalar)
                    {
                        yallamefxhalar = true;
                        personaje_control.eventosfeel.halar();
                        personaje_control.camarografo.SetTrigger("zoom");

                        if (miprimeravez)
                        {
                            personaje_control.control_vibraciones.comenzar.vibracion1();
                            controlNivel.canvas_inicio.SetActive(false);
                            controlNivel.yacomenzoeljuego = true;
                            controlNivel.canvas_principal.SetActive(true);
                            miprimeravez = false;
                        }
                    }
                }
            }

        }

        else if (Input.GetMouseButtonUp(0))
        {
            yallamefxhalar = false;
            if (ini_clic)
            {
                mostrarflecha = false;
                personaje_control.joystick.transform.localScale = Vector2.zero;
                pos_fin_clic = Input.mousePosition;
                mover();
                ini_clic = false;
                activarflecha = false;
                CancelInvoke("activarFlecha");
                Invoke("activarFlecha", 0.4f);
                personaje_control.eventosfeel.salto();
                personaje_control.camarografo.SetTrigger("zoom out");
                CancelInvoke("activarVibracion");
                Invoke("activarVibracion", 0.6f);
                personaje_control.vibration.m_ImpulseDefinition.m_AmplitudeGain = personaje_control.valor_vibracion_salto;
                personaje_control.vibration.GenerateImpulse();
            }
        }

        if (ini_clic)
        {
            float a = (Input.mousePosition.y - pos_ini_clic.y);
            float b = (Input.mousePosition.x - pos_ini_clic.x);

            if (a < 0)
            {
                float pendiente = a / b;
                float anguloRadianes = Mathf.Atan(pendiente);
                float anguloGrados = anguloRadianes * Mathf.Rad2Deg;


                var angles = transform.rotation.eulerAngles;
                angles.x = -angulo_lanzamiento_base + 90;
                angles.y = (Mathf.Abs(anguloGrados)) - 90;
                if (anguloGrados < 0)
                {
                    angles.y = anguloGrados + 90;
                }
                else
                {
                    angles.y = anguloGrados - 90;
                }
                angles.z = 0;
                angles.y = -angles.y;
                personaje_control.flecha.transform.rotation = Quaternion.Euler(angles);
            }
        }
        else
        {
            if (activarflecha)
            {
                var angles = transform.rotation.eulerAngles;
                angles.x = -angulo_lanzamiento_base + 90;
                angles.z = 0;
                angles.y = 0;
                personaje_control.flecha.transform.rotation = Quaternion.Euler(angles);
            }
        }

        if ((estacercadelsuelo() || activador_movimiento || mostrarflecha) && !controlNivel.yaganoelpersonaje)
        {
            if (mepuedomover)
            {
                if (activarflecha)
                {
                    if (!personaje_control.flecha.activeSelf)
                    {
                        personaje_control.flecha.SetActive(true);
                    }
                }
                else
                {
                    if (personaje_control.flecha.activeSelf)
                    {
                        personaje_control.flecha.SetActive(false);
                    }
                }
            }
            else
            {
                if (personaje_control.flecha.activeSelf)
                {
                    personaje_control.flecha.SetActive(false);
                }
            }
        }
        else
        {
            if (personaje_control.flecha.activeSelf)
            {
                personaje_control.flecha.SetActive(false);
            }
        }
    }
    public void mover()
    {
        float a = (pos_fin_clic.y - pos_ini_clic.y);
        float b = (pos_fin_clic.x - pos_ini_clic.x);

        if (a < 0)
        {
            Vector2 mouse = new Vector2(pos_ini_clic.x - pos_fin_clic.x, pos_ini_clic.y - pos_fin_clic.y);

            float distanciaclicsy = Vector3.Distance(pos_ini_clic, pos_fin_clic);

            float unidadesmovidas = distanciaclicsy / unidad_pantalla_ajustada;

            if (unidadesmovidas > 1.5f)
            {
                unidadesmovidas = 1.5f;
            }


            Vector3 force = new Vector3(mouse.normalized.x, valor_vector_angulo_lanzamiento_base, mouse.normalized.y);

            esfera.AddForce(force * fuerzaActual * 100 * unidadesmovidas);

        }
    }
    IEnumerator activarMovimiento(bool estado, float segundosespera)
    {
        yield return new WaitForSeconds(segundosespera);
        activador_movimiento = estado;
    }
    public void lanzarPorRampa()
    {
        if (mepuedomover)
        {
            esfera.velocity = Vector3.zero;
            Vector3 force = new Vector3(0, 1, 1);
            //esfera.AddForce(force.normalized * fuerzaBase * 100 * porcentajeSalto);
            esfera.velocity = force.normalized * fuerzaBase * 0.14f * porcentajeSalto;
            personaje_control.eventosfeel.lanzarrampa();
            personaje_control.control_vibraciones.rampa.vibracion1();
        }
    }
    public void actualizarDatos(float fuerza, float porcentajeSaltop, float porcentajereduccionp, float distaniasuelop, float angulop)
    {
        fuerzaBase = fuerza;
        fuerzaActual = fuerza;

        porcentajeReduccion = porcentajereduccionp;
        porcentajeSalto = porcentajeSaltop;
        distacia_del_suelo = distaniasuelop;
        angulo_lanzamiento_base = angulop;

        personaje_control.personaje_material.material = personaje_control.limpio;

        valor_vector_angulo_lanzamiento_base = angulo_lanzamiento_base / 45f;
    }
    public void chocarcollisonador(Collision collision)
    {
        if (!yachoquemultiplicador)
        {
            personaje_control.control_vibraciones.multiplicador.vibracion1();
            yachoquemultiplicador = true;
            ContactPoint contacto = collision.contacts[0];
            GameObject instanciado = Instantiate(personaje_control.eventosfeel.par_monedas_multiplicador, contacto.point, Quaternion.identity) as GameObject;

            personaje_control.eventosfeel.chocarContraMultiplicador();

            controlNivel.monedas_nivel = (controlNivel.monedas_nivel * (collision.gameObject.GetComponent<MultiplicadorIde>().ide));
            controlNivel.txt_monedas_nivel.text = (controljuego.monedas_totales + controlNivel.monedas_nivel).ToString();


            StopAllCoroutines();
            mepuedomover = false;
        }
    }
    public void metodoColisionconpiso(Collision collision)
    {
        if (vibrar_colision)
        {
            vibrar_colision = false;
            personaje_control.eventosfeel.tocarpiso();
            personaje_control.vibration.m_ImpulseDefinition.m_AmplitudeGain = personaje_control.valor_vibracion_piso;
            personaje_control.vibration.GenerateImpulse();
            personaje_control.control_vibraciones.choca_con_las_paredes.vibracion1();

            if (collision != null && personaje_control.nivelMancha > 0)
            {
                if (collision.gameObject.tag.Equals("PisoPiso"))
                {
                    ContactPoint contacto = collision.contacts[0];
                    GameObject instanciado = Instantiate(personaje_control.eventosfeel.par_mancha, new Vector3(contacto.point.x, contacto.point.y + 0.2f, contacto.point.z), Quaternion.identity) as GameObject;
                }
                else if (collision.gameObject.tag.Equals("PisoPisoDiagonal"))
                {
                    ContactPoint contacto = collision.contacts[0];
                    GameObject instanciado = Instantiate(personaje_control.eventosfeel.par_mancha_diagonal, new Vector3(contacto.point.x, contacto.point.y + 0.2f, contacto.point.z), Quaternion.identity) as GameObject;
                }
            }
        }
    }
    public bool estacercadelsuelo()
    {
        bool respuesta = false;

        int mask = (1 << 13) | (1 << 14) | (1 << 16) | (1 << 17) | (1 << 18) | (1 << 19);
        RaycastHit hit;
        var ray = new Ray(transform.position, Vector3.down); ;
        if (Physics.Raycast(ray, out hit, distacia_del_suelo, mask))
        {
            respuesta = true;
        }

        return respuesta;
    }
    public void activarFlecha()
    {

        if (!controlNivel.yaganoelpersonaje)
        {
            activarflecha = true;
        }

    }
    public void activarVibracion()
    {
        vibrar_colision = true;
    }
    public void activartocarrampa()
    {
        puedotocarrampa = true;
    }
}
