﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PersonajeControl : MonoBehaviour
{
    [HideInInspector]
    public List<GameObject> objetospegados;
    [HideInInspector]
    public List<GameObject> piscinasTocadas;
    [HideInInspector]
    public List<GameObject> objetosTocadosRegistro;



    // Fuerza
    public float fuerzaBase;
    public float porcentajeSalto;
    public float porcentajeReduccion;
    public float distacia_del_suelo;
    public float angulo_lanzamiento_base;

    // Control Mancha
    [HideInInspector]
    public int nivelMancha = 0;
    private int limiteAumento = 3;

    // Control Material Personaje
    public SkinnedMeshRenderer personaje_material;
    public Material limpio, nivel1mancha, nivel2mancha, nivel3mancha, nivel4mancha;

    // Otros
    public GAManager gamanager;
    public EventosFeel eventosfeel;
    public ControlVibraciones control_vibraciones;
    public MovimientoEsfera movimiento_esfera;

    //Camara
    public Animator camarografo;
    public CinemachineImpulseSource vibration;
    public float valor_vibracion_salto, valor_vibracion_piso;

    // Control de joystick
    public GameObject joystick;
    public GameObject flecha;



    // Start is called before the first frame update
    void Start()
    {
        movimiento_esfera.actualizarDatos(fuerzaBase, porcentajeSalto, porcentajeReduccion, distacia_del_suelo, angulo_lanzamiento_base);
        objetospegados = new List<GameObject>();
    }
    public void despegarObjetos()
    {
        for (int i = 0; i < objetospegados.Count; i++)
        {
            objetospegados[i].GetComponent<CharacterJoint>().breakForce = 0;
            objetospegados[i].GetComponent<CharacterJoint>().breakTorque = 0;

        }
        objetospegados = new List<GameObject>();
    }

    public bool tengoPiscinaDentro(GameObject piscinap)
    {
        bool respuesta = false;
        for (int i = 0; i < piscinasTocadas.Count; i++)
        {
            if (piscinasTocadas[i] == piscinap)
            {
                respuesta = true;
                return respuesta;
            }
        }
        return respuesta;
    }
    public bool tengoObjetoRegistradoDentro(GameObject objetop)
    {
        bool respuesta = false;
        for (int i = 0; i < objetosTocadosRegistro.Count; i++)
        {
            if (objetosTocadosRegistro[i] == objetop)
            {
                respuesta = true;
                return respuesta;
            }
        }
        return respuesta;
    }


    ////////////////////////////////////////// Mancha ///////////////////////////////
    public void manchar()
    {
        movimiento_esfera.fuerzaActual = fuerzaBase * porcentajeReduccion;
        //eventosfeel.splashcamera.SetTrigger("manchar");
        if (nivelMancha == 0)
        {
            personaje_material.material = nivel1mancha;
        }
        else if (nivelMancha == 1)
        {
            personaje_material.material = nivel2mancha;
        }
        else
        {
            personaje_material.material = nivel3mancha;
        }
        if (nivelMancha < 3)
        {
            nivelMancha++;
            limiteAumento++;
        }
    }
    public void aumentarPorTocarAgua()
    {
        movimiento_esfera.fuerzaActual = fuerzaBase;
        if (nivelMancha == 4)
        {
            personaje_material.material = nivel3mancha;
        }
        else if (nivelMancha == 3)
        {
            personaje_material.material = nivel2mancha;
        }
        else if (nivelMancha == 2)
        {
            personaje_material.material = nivel1mancha;
        }
        else
        {
            personaje_material.material = limpio;
        }
        if (nivelMancha <= 3 && nivelMancha > 0)
        {
            nivelMancha--;

        }
    }
    public void caerEnPiscinaMancha()
    {
        movimiento_esfera.fuerzaActual = fuerzaBase * porcentajeReduccion;
        personaje_material.material = nivel3mancha;
        nivelMancha = 3;
    }
    public void caerEnPiscinaAgua()
    {
        movimiento_esfera.fuerzaActual = fuerzaBase;
        personaje_material.material = limpio;
        nivelMancha = 0;
        despegarObjetos();
    }
}
